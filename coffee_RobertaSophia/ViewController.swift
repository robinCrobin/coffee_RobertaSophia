//
//  ViewController.swift
//  coffee_RobertaSophia
//
//  Created by COTEMIG on 16/04/44 AH.
//

import UIKit
import Alamofire
import Kingfisher

struct Coffee: Decodable {
    let file: String
}

class ViewController: UIViewController {
    
    @IBOutlet weak var btn_recarregar: UIButton!
    @IBOutlet weak var img_coffee: UIImageView!
    
    func getImageCoffee(){
        AF.request("https://coffee.alexflipnote.dev/random.json").responseDecodable(of: Coffee.self) { response in
            if let coffee = response.value {
                self.img_coffee.kf.setImage(with: URL(string: coffee.file))
            }
        }
    }

    @IBAction func recarregar_imagem(_ sender: Any) {
        getImageCoffee()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getImageCoffee()
    }
}

